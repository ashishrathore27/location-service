# LOCATION-SERVICE

Minimal [Spring Boot](http://projects.spring.io/spring-boot/) 

## Requirements

For building and running the application you need:

- [JDK 11](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
- [Maven ](https://maven.apache.org)

## Running the application locally
   
There are several ways to run a Spring Boot application on your local machine. One way is to execute the `main` method in the `com.gatewaygroup.locationservice.LocationServiceApplication` class from your IDE.

Alternatively you can use the [Spring Boot Maven plugin](https://docs.spring.io/spring-boot/docs/current/reference/html/build-tool-plugins-maven-plugin.html) like so:

```shell
mvn spring-boot:run
```

## API-DOC
http://localhost:8080/gatewaygroup/api/api-doc

## Postman collection
src/test/resources/postman/location-service.postman_collection.json
