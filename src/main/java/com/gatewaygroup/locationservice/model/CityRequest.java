package com.gatewaygroup.locationservice.model;

public class CityRequest {
    private String cityName;

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }
}
